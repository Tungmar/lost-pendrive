<?php

$config['settings']['gameWorldPath'] = '../game/';
$config['settings']['gameWorlds'] = [
    'default' => 'LostPendrive.0',
    'demo' => 'LostPendrive.0',
];

$config['settings']['gameSavePath'] = '../save/';
$config['settings']['gameLogFile'] = '../save/game.log';

$config['settings']['displayErrorDetails'] = true;
$config['settings']['addContentLengthHeader'] = false;
