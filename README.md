# Lost Pendrive text adventure game

The demo level of the game can be played at: https://heldlp.uber.space/#demo


## Run the game localy in docker

1. Clone the repo localy and switch to the root directory of the repo.
2. Run once:
   ```shell
   composer install
   chmod a+w save/
   cp config_dist.php config.php
   curl "https://code.jquery.com/jquery-3.3.1.min.js" > html/vendor/jquery-3.3.1.min.js
   ```
3. To start the docker containers, run:
   ```shell
   docker-compose -f docker/docker-compose.yml up
   ```
4. In a browser open http://localhost:8080


## Game File Syntax

* Entries with a integer key (so with out a specific text key) are texts which are shown to the player.
  If the entry is a array, the first element is considered the text and the second the type of the message.
* Entries with a string key describe a task the engine should execute. The possible tasks are listed below.


### alias
The command is a alias of an other command.

Syntax:

'alias' => 'other command'


### go
Go to an other room. In the other room the look command will be executed.

Syntax:

'go' => 'other room'


### more
As array can only have one element with the same key name, you can use this
command to ad additional task of the same type.

Syntax:

'more' => [<other tasks>]

Example:

['alias' => 'com1', 'more' => ['alias' => 'com2']]
This will execute com1 and then com2.


### set
Set a variable to true. Variables can only be true or false.

Syntax:

'set' => 'variable_name'


### unset
Set a variable to false. Variables can only be true or false.

Syntax:

'unset' => 'variable_name'


### if
Checks if a variable is set to true. If it is not set, it is considered false.

Syntax:

This task needs a array with exactly 3 elements as value:

'if' => ['variable', 'then block', 'else block']


### if_item
Checks if the player has a specific item in his inventory.

Syntax:

This task needs a array with exactly 3 elements as value:

'if_item' => ['item', 'then block', 'else block']


### one_of
This will execute randomly just one of the given tasks.

Syntax:

'one_of' => ['one', 'or two']


### get_item
Will add a item to the inventory.

Syntax:

'get_item' => 'item'


### lose_item
Will remove a item from the inventory.

Syntax:

'lose_item' => 'item'

### end_game
Will end the game. The parameter will indicate in what state the game was
ended (win, loose or what ever).

Syntax:

'end_game' => 'state'
