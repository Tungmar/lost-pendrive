<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$config = new \Slim\Container();
require '../config.php';

$config['errorHandler'] = function ($config) {
    return function (Request $request, Response $response, Exception $exception) use ($config){
        $data = ['status' => 'error', 'message' => $exception->getMessage()];
        $code = $exception->getCode();
        $config->logger->error($data['message'], ['code' => $code]);
        return $response->withJson($data, $code < 400 ? 500 : $code);
    };
};
$config['notFoundHandler'] = function ($config) {
    return function (Request $request, Response $response) use ($config){
        $data = ['status' => 'error', 'message' => 'Can\'t find any thing like that.'];
        return $response->withJson($data, 404);
    };
};
$config['notAllowedHandler'] = function ($config) {
    return function (Request $request, Response $response, $methods) use ($config){
        $data = ['status' => 'error', 'message' => 'Method must be one of: ' . implode(', ', $methods)];
        return $response->withJson($data, 405)
            ->withHeader('Allow', implode(', ', $methods));
    };
};

$config['logger'] = function($config){
    $logger = new \Monolog\Logger('gameLogger');
    $file_handler = new \Monolog\Handler\StreamHandler($config['settings']['gameLogFile']);
    $file_handler->setFormatter(new \Monolog\Formatter\LineFormatter("%datetime% %level_name% %message% %context%\n", 'Y-m-d'));
    $logger->pushHandler($file_handler);
    return $logger;
};

$app = new \Slim\App($config);

$app->get('/cheater/game/start[/{name}]', 'GameCreator');
$app->post('/cheater/command', 'GameMaster');

$app->run();
