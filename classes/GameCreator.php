<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface;

class GameCreator
{
    protected $container;
    protected $settings;
    protected $gameKey;
    protected $gameWorld;
    protected $messages;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->settings = $container->get('settings');
        $this->messages = new GameMessagesContainer();
    }

    public function __invoke(Request $request, Response $response, Array $args){
        $gameWorlds = $this->settings['gameWorlds'];
        $gameWorld = $gameWorlds['default'];
        if(isset($args['name'])){
            if(isset($gameWorlds[$args['name']])){
                $gameWorld = $gameWorlds[$args['name']];
            }
            else{
                $this->messages->add('The selected game doesn\'t exist. I selected the default one for you.', 'error');
            }
        }

        $this->createGame($gameWorld);
        $data['status'] = 'success';
        $data['data']['gameKey'] = $this->gameKey;
        $data['data']['messages'] = $this->messages;
        return $response->withJson($data);
    }

    protected function createGame($gameWorld){
        $gameSave = new GameSave($this->settings);
        $gameSave->set('gameWorld', $gameWorld);
        $gameSave->set('gameStartDate', (new DateTime())->format('c'));
        $gameSave->set('gameStatus', 'new');
        $gameSave->set('gameMoves', 0);
        $this->gameWorld = new GameWorld($this->settings, $gameSave, $this->messages);
        $this->gameWorld->load();
        $this->gameWorld->startNewGame();
        do{
            $gameKey = $this->createToken(63);
        }
        while(!$gameSave->create($gameKey));

        $this->container->logger->info(
            'New game started',
            ['world' => $gameWorld]
        );

        $this->gameKey = $gameKey;
    }

    /**
     * Creates a cryptographically secure random string.
     * @author Scott
     * @see https://stackoverflow.com/a/13733588/1629120
     * @param int $length
     * @return string
     */
    protected function createToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }

    /**
     * Creates a cryptographically secure random string.
     * @author erjiang
     * @see https://stackoverflow.com/a/13733588/1629120
     * @param int $entropy Use a number dividable by 3 for better results
     * @return string The output string will be aprox 35% longer than $entropy
     */
    protected function createTokenFast($entropy){
        return base64_encode(openssl_random_pseudo_bytes($entropy));
    }
}
