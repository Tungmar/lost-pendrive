<?php

class GameMessagesContainer implements JsonSerializable
{
    public $messages = [];

    public function add($message, $type = null){
        if(is_array($message)){
            foreach($message as $m){
                $this->store($m, $type);
            }
        }
        else{
            $this->store($message, $type);
        }
    }

    protected function store($message, $type = null){
        if($type){
            $this->messages[] = [$message, $type];
        }
        else{
            $this->messages[] = $message;
        }
    }

    public function jsonSerialize(){
        return $this->messages;
    }
}
