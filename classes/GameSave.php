<?php

class GameSave
{
    protected $settings;
    protected $file;
    protected $data = [];
    protected $gameKey;

    public function __construct($settings) {
        $this->settings = $settings;
    }

    private function getFilePath($gameKey){
        if(!preg_match('/^[a-zA-Z0-9]{63}$/', $gameKey)){
            throw new Exception('Invalid game key', 400);
        }
        return $this->settings['gameSavePath'].$gameKey.'.save';
    }

    public function create($gameKey){
        if(!is_writable($this->settings['gameSavePath'])){
            throw new Exception("Save path [{$this->settings[gameSavePath]}] is not writeable.");
        }
        $file = $this->getFilePath($gameKey);
        if(is_file($file)){
            return false;
        }
        $this->file = $file;
        $this->save();

        $this->gameKey = $gameKey;
        return true;
    }

    public function save(){
        $data = json_encode($this->data);
        if(false === file_put_contents($this->file, $data, LOCK_EX)){
            throw new Exception("Could not create file [{$this->file}]");
        }
    }

    public function load($gameKey){
        $this->file = $this->getFilePath($gameKey);
        if(!is_readable($this->file)){
            throw new Exception('Saved game dos not exist (anymore).', 410);
        }
        if(!($data = file_get_contents($this->file))){
            throw new Exception('Could not load saved game.');
        }
        $this->data = json_decode($data, true);
    }

    public function set($key, $value = false){
        if(is_array($key)){
            $this->data = array_merge($this->data, $key);
        }
        else{
            $this->data[$key] = $value;
        }
    }

    public function mod($key, $mod, $value = 1){
        if(!isset($this->data[$key])){
            $this->data[$key] = 0;
        }
        switch($mod){
            case '+':
                $this->data[$key] += $value;
                break;
            case '-':
                $this->data[$key] -= $value;
                break;
        }
    }

    public function get($key){
        if(!isset($this->data[$key])){
            return null;
        }
        return $this->data[$key];
    }

    public function add($key, $value){
        if(!is_array($this->data[$key])){
            $this->data[$key] = [$value];
        }
        else{
            $this->data[$key][] = $value;
        }
    }

    public function remove($key, $value){
        if(($index = array_search($value, $this->data[$key])) !== false){
            unset($this->data[$key][$index]);
        }
    }

    public function has($key, $value){
        return in_array($value, $this->data[$key]);
    }
}
