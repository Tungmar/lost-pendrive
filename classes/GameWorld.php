<?php

class GameWorld
{
    protected $settings;
    protected $gameSave;
    protected $world;
    protected $messages;

    public function __construct($settings, GameSave $gameSave, GameMessagesContainer $messages){
        $this->settings = $settings;
        $this->gameSave = $gameSave;
        $this->messages = $messages;
    }

    public function load(){
        $worldFile = $this->settings['gameWorldPath'].$this->gameSave->get('gameWorld').'.php';
        if(!is_readable($worldFile)){
            throw new Exception("Game file is not readable: [$worldFile]", 40);
        }
        $this->world = $this->loadFile($worldFile);
    }

    private function loadFile($file){
        return include $file;
    }

    public function startNewGame(){
        $this->gameSave->set($this->world['startSettings']);
        $this->messages->add($this->world['startMessages']);
    }

    public function roomCommand($command){
        $room = $this->getCurrentRoom();
        if(isset($room[$command])){
            $this->doTasks($room[$command]);
            return true;
        }
        return false;
    }

    public function allRoomsCommand($command){
        if(isset($this->world['allRooms'][$command])){
            $this->doTasks($this->world['allRooms'][$command], 'allRoomsCommand');
            return true;
        }
        return false;
    }

    protected function doTasks($tasks, $commandMethod = 'roomCommand'){
        if(!is_array($tasks)){
            $tasks = array($tasks);
        }
        foreach($tasks as $task => $param){
            # Elements with a integer key are just text to output.
            if(is_int($task)){
                if(is_array($param)){
                    $this->messages->add($param[0], $param [1]);
                }
                else{
                    $this->messages->add($param);
                }
                continue;
            }

            switch($task){
                case 'go':
                    $this->gameSave->set('gameRoomLast', $this->gameSave->get('gemeRoomCurrent'));
                    $this->gameSave->set('gemeRoomCurrent', $param);
                    $this->roomCommand('look');
                    break 2;
                case 'alias':
                    $this->$commandMethod($param);
                    break;
                case 'one_of':
                    $this->doTasks($param[array_rand($param)]);
                    break;
                case 'more':
                    $this->doTasks($param);
                    break;
                case 'if':
                    $this->doIfTask($param);
                    break;
                case 'set':
                    $this->gameSave->set($param, true);
                    break;
                case 'unset':
                    $this->gameSave->set($param, false);
                    break;
                case 'if_item':
                    $this->doIfHasTask('gameItems', $param);
                    break;
                case 'get_item':
                    $this->gameSave->add('gameItems', $param);
                    break;
                case 'lose_item':
                    $this->gameSave->remove('gameItems', $param);
                    break;
                case 'end_game':
                    $this->doEndGameTask($param);
                    break;
                default:
                    $room = $this->gameSave->get('gemeRoomCurrent');
                    throw new Exception("There is a unknown task [$task] in room [$room].", 51);
            }
        }
    }

    protected function doIfTask($param){
        if(!is_array($param) || count($param) != 3){
            $room = $this->gameSave->get('gemeRoomCurrent');
            throw new Exception("There is a 'if' task with wrong param in room [$room].", 52);
        }
        if($this->gameSave->get($param[0])){
            $this->doTasks($param[1]);
        }
        else{
            $this->doTasks($param[2]);
        }
    }

    protected function doIfHasTask($key, $param){
        if(!is_array($param) || count($param) != 3){
            $room = $this->gameSave->get('gemeRoomCurrent');
            throw new Exception("There is a 'if has' task with wrong param in room [$room].", 53);
        }
        if($this->gameSave->has($key, $param[0])){
            $this->doTasks($param[1]);
        }
        else{
            $this->doTasks($param[2]);
        }
    }

    protected function doEndGameTask($reason){
        if(!isset($this->world['endMessages'][$reason])){
            throw new Exception("There is a unknown end game reason [$reason]", 54);
        }

        $this->gameSave->set('gameStatus', 'finished');
        $this->doTasks($this->world['endMessages'][$reason]);
    }

    protected function getCurrentRoom(){
        $current = $this->gameSave->get('gemeRoomCurrent');
        if(!isset($this->world['rooms'][$current])){
            throw new Exception("Could not load current room [$current].", 50);
        }
        return $this->world['rooms'][$current];
    }

    public function getInventory(){
        return $this->gameSave->get('gameItems');
    }
}
