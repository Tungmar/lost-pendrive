<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface;

class GameMaster
{
    protected $container;
    protected $settings;
    protected $gameSave;
    protected $gameWorld;
    protected $messages;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->settings = $container->get('settings');
        $this->messages = new GameMessagesContainer();
    }

    public function __invoke(Request $request, Response $response, Array $args){
        $input = $request->getParsedBody();

        $this->gameSave = new GameSave($this->settings);
        $this->gameSave->load($input['gameKey']);
        if('finished' == $this->gameSave->get('gameStatus')){
            throw new Exception('This saved game is already finished.', 409);
        }
        $this->gameSave->set('gameStatus', 'running');
        $this->gameWorld = new GameWorld($this->settings, $this->gameSave, $this->messages);
        $this->gameWorld->load();

        $this->command($input['typed']);

        $data['data']['gameStatus'] = $this->gameSave->get('gameStatus');
        if('finished' == $data['data']['gameStatus']){
            $this->listStatistic('new');
            $this->messages->add('Type <em>game start</em> to start a new game.', 'new');
        }

        $this->gameSave->save();
        $data['status'] = 'success';
        $data['data']['messages'] = $this->messages;
        if(($ob = ob_get_clean())){
            $data['data']['debug'] = $ob;
        }
        return $response->withJson($data);
    }

    protected function command($command){
        switch(true){
            case $this->gameWorld->roomCommand($command):
                break;
            case $this->gameWorld->allRoomsCommand($command):
                break;
            case ('inventory' == $command):
                $this->listInventory();
                break;
            case ('statistic' == $command):
                $this->listStatistic();
                break;
            case ('show location' == $command):
                $this->messages->add('You are in room '.$this->gameSave->get('gemeRoomCurrent').'.');
                break;
            case (strpos($command, 'bug ') === 0):
                $this->log('Bug message: '.$command);
                $this->messages->add('Thank you for your input! I took a note about that.');
                return;
            default:
                $this->log('New command: '.$command);
                $this->messages->add('I don\'t understand that.', 'confused');
                return;
        }
        $this->gameSave->mod('gameMoves', '+', 1);
        $this->gameSave->set('gameLastCommand', $command);
    }

    protected function listInventory(){
        $this->messages->add('You carry with you:');
        $inventory = $this->gameWorld->getInventory();
        if($inventory){
            $this->messages->add(implode(', ', $inventory));
        }
        else{
            $this->messages->add('Nothing...');
        }
    }

    protected function listStatistic($type = null){
        $startDate = new DateTime($this->gameSave->get('gameStartDate'));
        $time = (new DateTime())->diff($startDate);
        $duration = '';
        foreach(['y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second'] as $char => $unit){
            if($time->$char > 0){
                $duration .= $time->$char.' '.$unit.($time->$char!=1 ? 's':'').' ';
            }
        }
        $moves = $this->gameSave->get('gameMoves');
        $this->messages->add("In the {$duration}of your game you made $moves move".($moves!=1?'s':'').'.', $type);
    }

    protected function log($message){
        $this->container->logger->notice(
            $message,
            [
                'world' => $this->gameSave->get('gameWorld'),
                'room' => $this->gameSave->get('gemeRoomCurrent'),
                'command' => $this->gameSave->get('gameLastCommand'),
            ]
        );
    }
}
