<?php

/**
 * A new game is initialized with this variables set.
 */
$startSettings = [
    'gemeRoomCurrent' => 'entry_wall_e', // Name of the room where the player starts.
    'gameItems' => ['flashlight'], // Items the player carries in the beginning
];

/**
 * This messages are shown to the player when the game starts.
 */
$startMessages = [
    'Great! If you like to get a introduction to the game, type <em>help start</em>. And with <em>help</em> I can give you some clues.',
    ['But now for the story:', 'new'],
    ['You are walking along a stream to find just this one more geocache. As you pass by an old building a robot walks towards you. He stops right in front of you and looks with sad eyes at you.', 'new'],
];

/**
 * All the rooms in the game. Rooms are just places where the player can go to.
 * The rooms are built from room name and comands avilable to the player in the room.
 * Each command has a set of tasks wich are executed when this command is choosen.
 *
 * Example:
 *  'room_name'
 *    'command' => Tasks
 *
 * Tasks:
 * Tasks are tings the game does when the command is executed. The type of task is written befor the arrow (=>)
 *   No type:    This is a text shown to the player
 *   go:         Switch to a other room
 *   alias:      Run a other command instead
 *   more:       Additional tasks to execute
 *   one_of:     Randomly choose one of the tasks inside [] separated by a comma (,)
 *   set:        Set a variable to true
 *   unset:      Set a variable to false
 *   if:         3 parameters inside [] separated by a comma (,):
 *                - variable to check
 *                - tasks to execute if the variable is true
 *                - tasks to execute if the variable is false
 *   get_item:   Add item to the users inventory
 *   loose_item: Remove item form the users inventory
 *   if_item:    3 parameters inside [] separated by a comma (,):
 *                - item to check
 *                - tasks to execute if the item is in the inventory
 *                - tasks to execute if the item is not in the inventory
 *   end_game:   End the game
 *
 */
$rooms = [
    'entry_wall_e' => [
        'look' => 'You standing in front of a old rusty robot. He is looking with sad eyes at you.',
        'look around' => 'To east the path leads off the scene. In the west is a path between the wall and the river. In the north is the river.',
        'run' => 'The robot looks friendly. No need to run away.',
        'look robot' => 'The robot is old and rusty. He probably can talk.',
        'talk robot' => 'The robot talks with a cracking voice: "Hello. Pleep. Can you find my pendrive? It has my home address stored. With out it I\'m lost. Pleep." The robot points with one arm to the west: "A rabbit took it and run of into the hole over there. Pleep. It would be very nice of you if you can bring it back to me. Pleep."',
        'talk yes' => 'Robot: "Thank you! Pleep. Pleep". The robot tries to smile but fails miserably.',
        'talk no' => 'The robot looks more sad. How is that possible?',
        'talk hole' => ['You tell the robot about the hole blocked by the bars.', 'if' => ['entrance_bars_bent',
            'Robot: "I bend them for you. I\'m good at bending. Pleep."',
            ['if' => ['entry_robot_powered',
                ['Robot: "I\'m well powered. Pleep. I can bend that bars open for you." It moves over to the hole, grabs two bars in the middle and bends them outwards. There is now a wide gab between the bars. Then the robot returns to you: "All done. Pleep."', 'set' => 'entrance_bars_bent'],
                'Robot: "I feel to weak to help you. Pleep. My charging station is at home and I don\'t know how to get home with out my pendrive. Pleep. Pleep."',
            ]],
        ]],
        'talk robot hole' => ['alias' => 'talk hole'],
        'talk robot bars' => ['alias' => 'talk hole'],
        'talk bars' => ['alias' => 'talk hole'],
        'ask robot' => ['alias' => 'talk hole'],
        'ask robot hole' => ['alias' => 'talk hole'],
        'ask robot bars' => ['alias' => 'talk hole'],
        'talk pendrive' => 'Robot: "It is a small device and it is very important to me."',
        'talk robot pendrive' => ['alias' => 'talk pendrive'],
        'give battery' => ['if_item' => ['battery',
            ['You offer a battery to the robot. It puts it in its mouth and mumbles: "Thank you! Pleep. I like to chew them."', 'On the robots chest, a red light flashes up.', 'lose_item' => 'battery', 'set' => 'entry_robot_powered'],
            'The robot looks surprised at you: "Yes! Pleep. I like batteries. But you don\'t have one.',
        ]],
        'give robot battery' => ['alias' => 'give battery'],
        'give pendrive' => ['if_item' => ['pendrive',
            ['You give the pendrive to the robot. It puts it in its mouth and mumbles: "Thank you! Pleep. I like to chew ... oh! That\'s my drive! Finally I will find my way home. Thank you! Your amazing!"', 'lose_item' => 'pendrive', 'end_game' => 'win'],
            'The robot looks surprised at you: "Yes! Pleep. I\'m looking for me pendrive. Do you have it?"',
        ]],
        'give robot pendrive' => ['alias' => 'give pendrive'],
        'take robot' => 'It is bigger than you and it may take you instead.',
        'kill robot' => 'It is bigger than you and it may not like that.',

        'go north' => ['go' => 'entry_river'],
        'go east' => ['You leave the robot behind you and wander off. He is sad but you live your life.', 'end_game' => 'survived'],
        'go south' => 'There is a wall here...',
        'go west' => ['go' => 'entry_wall_entrance'],
        'go river' => ['go' => 'entry_river'],
        'help' => ['if' => ['entry_robot_powered',
            'You can talk about a topic.',
            'Did you explore all directions (north, east, south, west, …)?',
        ]],
        'help robot' => 'That is so sweet of you! But how do you want to help the robot?',
    ],
    'entry_wall_entrance' => [
        'look' => 'You stand on a path which leads from west to east. On the south side of the path is a brick wall. There is a little hole at the bottom of the wall.',
        'look around' => 'To west the wall has a niche. To east you see a robot. In the north is the river.',
        'look wall' => 'A very long wall of a old, abandoned factory building. In the west you see a niche built in to the wall.',
        'look niche' => 'The niche is a bit in the west. You should go there and have a look.',

        'look hole' => ['A hole in the wall.', 'if' => ['entrance_bars_bent', 'You probably can squeeze through the now bent bars.', 'Metal bars block the way in for you but smal animals could go trough.']],
        'squeeze hole' => 'You try to make the hole smaller with your bare hands but soon you realize how pointless that is.',
        'look bars' => 'They are strong metal bars and anchored deep into the wall.',
        'take bar' => 'All the bars are deep embed in the wall and can\'t be removed.',
        'bend bar' => 'You are to weak to bend the metal bars.',
        'bend bars' => ['alias' => 'bend bar'],
        'break bar' => 'You are to weak to break the metal bars.',
        'break bars' => ['alias' => 'break bar'],
        'eat bars' => 'You probably would just lose your teeth...',
        'squeeze bars' => ['alias' => 'bend bars'],
        'go hole' => ['if' => ['entrance_bars_bent',
            ['You squeeze yourself through the bent bars into the hole.', 'go' => 'entry_tunnel_in'],
            'The entrance of the hole is blocked by metal bars.',
        ]],
        'squeeze through hole' => ['alias' => 'go hole'],
        'squeeze through bars' => ['alias' => 'go hole'],
        'crawl' => ['alias' => 'go hole'],
        'crawl hole' => ['alias' => 'go hole'],
        'crawl through hole' => ['alias' => 'go hole'],
        'enter hole' => ['alias' => 'go hole'],
        'go through bar' => ['alias' => 'go hole'],

        'go north' => ['go' => 'entry_river'],
        'go east' => ['go' => 'entry_wall_e'],
        'go south' => 'There is a wall here blocking your way. But the wall has a small hole and you may want to crawl in to it.',
        'go west' => ['go' => 'entry_wall_w'],
        'go wall' => 'You stand next to the wall.',
        'go robot' => ['You walk towards the robot.', 'go' => 'entry_wall_e'],
        'go river' => ['go' => 'entry_river'],
        'look river' => ['go' => 'entry_river'],
        'help' => ['if' => ['entrance_bars_bent',
            'Maybe you like to use your flashlight while you crawl in the hole.',
            'Who is a strong fellow?',
        ]],
    ],
    'entry_wall_w' => [
        'look' => 'You stand at a brick wall with a niche in it.',
        'look around' => 'To west the path leads off the scene. To east you see a robot in the distance. In the north is the river.',
        'look wall' => 'The wall was built with bricks a long time ago. Did I mention the niche in the wall right in front of you?',
        'yes' => 'Good.',
        'say yes' => ['alias' => 'yes'],
        'look niche' => 'In the niche lies a box with batteries.',
        'look box' => 'A cardboard box with batteries in it. I have no idea who put it there.',
        'look battery' => 'It is a cheap AA battery. But it looks new.',
        'open box' => 'The box is open.',
        'take box' => 'Not so greedy. Just take one battery.',
        'take betteries' => ['alias' => 'take box'],
        'take battery' => ['You take a battery.', 'get_item' => 'battery'],
        'go north' => ['go' => 'entry_river'],
        'go east' => ['go' => 'entry_wall_entrance'],
        'go south' => 'You don\'t fit in to the niche - now matter how hard you try.',
        'go west' => ['You wander of into a unknown future and live happily ever after.', 'end_game' => 'survived'],
        'go river' => ['go' => 'entry_river'],
        'go robot' => ['You walk towards the robot.', 'go' => 'entry_wall_e'],
    ],
    'entry_river' => [
        'look' => 'You stand at the shore of a wide, fast and deep river. Swimming in the river is for sure dangerous.',
        'look around' => 'In the north is the wide river. It is nice to stroll along the river in both directions. A bit away you see a robot and the wall.',
        'look river' => 'It is a wide and dangerous river.',
        'go north' => 'You face a river. Do you want to swim?',
        'go river' => ['You jump in to the river.', 'alias' => 'swim'],
        'swim' => ['The current tosses you away and you soon drown.', 'end_game' => 'dead'],
        'go east' => 'You wander a bit along the river.',
        'go south' => ['go' => 'entry_wall_entrance'],
        'go west' => 'You wander a bit upstream.', # Hint for directions
        'go robot' => ['You walk towards the robot.', 'go' => 'entry_wall_e'],
        'go wall' => ['go' => 'entry_wall_entrance'],
    ],

    'entry_tunnel_in' => [
        'look' => ['if' =>
            ['light',
                'Your flashlight illuminates a otherwise narrow dark tunnel built of bricks.',
                'You are in a narrow dark tunnel.'
            ], 'In the south you see some artificial light.',
        ],
        'look carrot' => 'You don\'t spot a carrot. Maybe it is to dark?',
        'go north' => ['You crawl backwards until you leave the tunnel.', 'go' => 'entry_wall_entrance'],
        'go south' => ['You crawl along the tunnel and at the end you squeeze through a barely noticeable hole in to a bigger room.', 'go' => 'fridge_room'],
        'exit tunnel' => ['alias' => 'go north'],
        'go light' => ['alias' => 'go south'],
    ],
    'entry_tunnel_out' => [
        'look' => ['if' =>
            ['light',
                'Your flashlight illuminates a otherwise narrow dark tunnel built of bricks. At one point you spot a little graffiti of a carrot.',
                'You are in a narrow dark tunnel.'
            ], 'In the north you see daylight.',
        ],
        'look carrot' => 'It is a orange graffiti on the wall and it is hard to spot.',
        'look graffiti' => ['alias' => 'look carrot'],
        'take graffiti' => 'Graffiti means a painting painted on to the wall. You can\'t take it.',
        'go north' => ['You crawl along the tunnel and into the daylight.', 'go' => 'entry_wall_entrance'],
        'go south' => ['You crawl backwards until you can stand up again.', 'go' => 'fridge_room'],
        'exit tunnel' => ['alias' => 'go south'],
        'go light' => ['alias' => 'go north'],
    ],

    'fridge_room' => [
        'look' => 'You are standing in a room with new, concrete walls. In the room is a big, metal box. There is a door to the east and a heavy door with big hinges to the west.',
        'look box' => ['if' => [ 'fridge_door',
            'The box makes sure the heavy door won\'t close by itself.',
            'This is a solid metal box and it looks like you can open it.',
        ]],
        'take box' => 'The box is bigger than you so you can\'t take it with you. But maybe you can drag it around in this room.',
        'open box' => ['You open the box and out comes a bad stench like rotten fish.',
            'if' => ['fridge_box_open', [], ['set' => 'fridge_box_open', 'There is a screwdriver in the box and you take it.', 'get_item' => 'screwdriver']],
            'The stench is to bad and so you close the box quickly.',
        ],
        'drag box' => ['if' => ['fridge_door',
            ['You move the box and let the heavy door close.', 'unset' => 'fridge_door'],
            ['You drag the box towards the heavy door, open the door and block it with the box. After a short check, you are sure that the door won\'t close by it self.', 'set' => 'fridge_door'],
        ]],
        'move box' => ['alias' => 'drag box'],

        'look door east' => 'The east door is a regular door except that it has a cat door built in near the bottom.',
        'look door west' => ['The west door is a massive chrome steel door with a big handle and a closing mechanism.',
            'if' => ['fridge_door',
                'The box makes sure it won\'t close by itself.',
                [],
            ],
            'more' => ['if' => ['fridge_label_taken',
                'It also has rubber put and the imprint of the removed label "Cold Boy".',
                'It also has rubber put and a label "Cold Boy".',
            ]],
        ],
        'look door heavy' => ['alias' => 'look door west'],
        'look door' => ['alias' => 'look door east', 'more' => ['alias' => 'look door west']],
        'look label' => 'It says "Cold Boy" in fancy letters.',
        'take label' => ['if' => ['fridge_label_taken', 'You already took it, remember?', 'It is fixed. Maybe some tool will help?']],
        'take label screwdriver' => ['if' => ['fridge_label_taken', ['alias' => 'take label'],
            ['if_item' => ['screwdriver',
                ['You take the label of the door.', 'set' => 'fridge_label_taken', 'get_item' => 'fridge label'],
                'Yes, a screwdriver could help. But you don\'t have one.',
            ]],
        ]],
        'look put' => 'Rubber on the edge of the door make the door close tight.',
        'take put' => 'It holds to strong.',
        'look mechanism' => 'It is a construction with a spring which will close the door.',
        'take mechanism' => 'It is bolted to the door.',
        'block mechanism' => 'Nice idea. But how?',
        'block mechanism screwdriver' => ['if_item' => ['screwdriver',
            'You try to block the closing mechanism with the screwdriver but you can\'t get it to hold securely.',
            'You don\'t have one.',
        ]],
        'block mechanism label' => ['if_item' => ['label',
            'The label is to weak and will probably just break.',
            [['Hu?', 'confused']],
        ]],
        'block mechanism box' => ['alias' => 'drag box'],

        'open door' => 'Which door do you like to open? Use <em>open door</em> <i>&lt;east or west&gt;</i>',
        'open door east' => 'You try to open the door but find it locked.',
        'open door west' => ['if' => [ 'fridge_door',
            'The door is already open and the box makes sure it won\'t close.',
            'You open the heavy door. But as you release the handle, the door closes automatically again.',
        ]],
        'unlock door' => [['This is odd! There is no keyhole!', 'confused']],
        'unlock door east' => ['alias' => 'unlock door'],
        'unlock door west' => 'The door has no lock.',

        'go north' => ['You find the almost hidden small hole in the wall and crawl into it.', 'go' => 'entry_tunnel_out'],
        'go east' => 'The door is locked.', # ['You open the door and step through.', 'go' => 'hallway1'],
        'go south' => 'There is just a concrete wall here. No trespassing possible!',
        'go west' => ['if' => [ 'fridge_door',
            ['go' => 'fridge'],
            ['You open the heavy door and step trough. As you let go of the door, it swings close. You hear a loud \'klink\' when the lock locks.', 'go' => 'fridge_closed'],
        ]],
        'help' => ['if' => ['fridge_box_open',
            'You can move things.',
            'The box looks odd, isn\'it?',
        ]],
    ],
    'fridge_closed' => [
        'look' => ['if' =>
            ['light',
                'You are standing in a empty fridge room. The walls, floor and ceiling are frozen. There is a door in the east. It is frozen too.',
                'You are standing in the dark. There is no light so to say.'
            ],
            'It is cold. Very cold. To cold for your gear in fact.',
        ],
        'go east' => ['The door is shut and there is no handle on this side.', 'if' =>
            ['cold',
                ['It is to cold in here and the hypothermia gets you with out you notice it. As there is no help and no heat source you fall to the floor and soon die.', 'end_game' => 'dead'],
                ['set' => 'cold'],
            ]
        ],
        'look door' => ['alias' => 'go east'],
        'open door' => ['alias' => 'go east'],
        'shout' => ['You shout for help but no one seams to hear you.', 'set' => 'cold'],
        'scream' => ['You scream in panic but no one seams to hear you.', 'set' => 'cold'],
        'help' => 'No one can help you.',
    ],
    'fridge' => [
        'look' => ['You are standing in a empty fridge room. The walls, floor and ceiling are frozen. There is light falling in from the open door in the east.', 'It is cold. Very cold. To cold for your gear in fact and you would like to leave the room and not touch any thing with you bare hands.'],
        'look door' => 'The door is on the inside frozen too. There is no handle on this side of the door.',
        'look wall' => 'The walls and the floor are covered with ice. In the left back corner you see a bigger chunk of ice.',
        'look floor' => ['alias' => 'look wall'],
        'look ice' => ['alias' => 'look wall'],
        'look corner' => ['You step closer to the corner and inspect the chunk of ice.',
            'if' => ['fridge_took_thing',
                'You see the ice you cracked open.',
                ['You see some thing in it.', 'set' => 'fridge_look_corner', 'alias' => 'wait'],
            ],
        ],
        'look ice chunk' => ['alias' => 'look corner'],
        'look chunk' => ['alias' => 'look corner'],
        'look thing' => 'You look some thing stupid in the corner.',
        'take ice' => ['That doesn\'t come off.', 'alias' => 'wait'],
        'take thing' => ['if' => ['fridge_took_thing',
            'You took it already...',
            ['if' => ['fridge_look_corner',
                ['That doesn\'t come off, it is under the ice.', 'alias' => 'wait'],
                [['Which thing you\'re talking about?', 'confused']]
            ]],
        ]],
        'take pendrive' => ['alias' => 'take thing'],
        'take thing screwdriver' => ['if' => ['fridge_took_thing',
            'You took it already...',
            ['if' => ['fridge_look_corner',
                ['if_item' => ['screwdriver',
                    ['You crush the ice with the screwdriver. That is not good for the screwdriver but you get the thing out. It is a little pendrive!', 'get_item' => 'pendrive', 'set' => 'fridge_took_thing'],
                    ['Yes, a screwdriver could help. But you don\'t have one.'],
                ]],
                ['Which thing you\'re talking about?', 'confused']
            ]],
        ]],
        'use screwdriver corner' => ['alias' => 'take thing screwdriver'],
        'use screwdriver ice' => ['alias' => 'take thing screwdriver'],
        'use screwdriver thing' => ['alias' => 'take thing screwdriver'],
        'use screwdriver' => 'Where or with what do you like to use the screwdriver?',

        'look ceiling' => ['The ceiling is covered with ice. In the middle is a lamp with a broken bulb.', 'alias' => 'wait'],
        'take lamp' => ['You try to remove the bulb. It is frozen stuck.', 'alias' => 'wait'],
        'take bulb' => ['alias' => 'take lamp'],
        'take lamp screwdriver' => ['That would just break it!', 'alias' => 'wait'],

        'wait' => ['if' => ['cold',
            ['It is too cold for you inside this fridge and you just run out!', 'alias' => 'go east'],
            ['You feel the coldness sneak in to your bones!', 'set' => 'cold'],
        ]],
        'go east' => ['You step out of the fridge and relax at the warm air here.', 'unset' => 'cold', 'go' => 'fridge_room'],
        'help' => ['if' => ['fridge_look_corner',
            'Do you have a useful tool?',
            'It is very cold in here so you may want to look around quickly and then go warm up outside before your next step.',
        ]],
    ],
];

/**
 * This are commands which are available to the user in all rooms.
 *
 * Example:
 *  'command' => Tasks
 *
 * Tasks:
 * The tasks are the same as for the rooms.
 *
 */
$allRooms = [
    'look' => 'There is nothing else of interest to see here.',
    'look around' => 'You can\'t see far here.',
    'talk' => 'Who do you want to talk to? Type <em>talk</em> <i>&lt;to whom&gt;</i>.',
    'take' => [['Take what?', 'confused']],
    'give' => [['Give what to whom?', 'confused']],
    'asdf' => [['Are you talking or bubling?', 'confused']],
    'magic' => 'Yes, magic makes things possible.',

    'go' => 'In which direction do you like to go? Try <em>go west</em>.',
    'go north' => 'There is no way to go north here.',
    'go east' => 'There is no way to go east here.',
    'go south' => 'There is no way to go south here.',
    'go west' => 'There is no way to go west here.',

    'look flashlight' => 'It is a nice, small but powerful flashlight which you always carry with you.',
    'light' => ['if' => ['light',
        ['You turn off your flashlight.', 'unset' => 'light'],
        ['You turn on your flashlight. Maybe you see now some thing more if you look?', 'set' => 'light'],
    ]],
    'use flashlight' => ['alias' => 'light'],
    'use battery flashlight' => 'Your flashlight holds already a well charged accu.',
    'take accu flashlight' => 'You consider a working flashlight as too important to do that.',

    'help' => ['one_of' => [
        'Did you explore all directions (north, east, south, west, …)?',
        'Some commands need multiple words.',
        '<em>look</em> around and look for clues.',
        '<em>look around</em> and look for clues.',
        'With <em>look <i>&lt;thing&gt;</i></em> you can have a closer look at some thing.',
        'The right words for the commands are often used in the text.',
        'With <em>help start</em> you can read the introduction again.',
        ['one_of' => [
            'Hint: You don\'t need to type words like \'in\', \'at\' or \'with\'.',
            'You can <em>take</em> stuff.',
            'You can <em>move</em> stuff.',
            'You can <em>give</em> stuff.',
        ]],
    ]],
    'help start' => [
        'This game works by typing in simple command and reading the answers.',
        'I\'m your avatar in the world of this game. I will tell you what you see and what\'s  happening. I will also act on your behalf. You can tell me with simple commands what I, as your avatar, should do. I do some times talk much, but I don\'t understand much. So you have to use simple commands or I won\'t understand you.',
        ['Here are some of the commands which I do understand:', 'new'],
        '<em>look</em> : I describe to you, again, where you currently are.',
        '<em>look around</em> : I will describe to you what is to see in the distance.',
        '<em>look</em> <i>&lt;thing&gt;</i> : Maybe I can tell you more about that thing.',
        '<em>go</em> <i>&lt;direction&gt;</i> : Walk in that direction or towards that thing.',
        '<em>talk</em> <i>&lt;thing&gt;</i> <i>[&lt;topic&gt;]</i> : Talk with the thing. The topic is not mandatory.',
        '<em>inventory</em> : I tell you what junk you carry around.',
        '<em>help</em> : Sometimes I may have a useful clue for you.',
        ['There are also other commands which work in some situations.', 'new'],
    ],

    #'go work' => ['go' => 'fridge_room'],
];

/**
 * The end messages are shown to the player if the game is finished. It depends on the reason the game finishes.
 */
$endMessages['survived'] = [
    ['This is the end of your adventure. You can start a new game and try to achieve more.', 'new'],
];
$endMessages['win'] = [
    ['You managed to bring the robots pendrive back and win the game! Congratulation!', 'new'],
];
$endMessages['dead'] = [
    ['I\'m sorry that it had to end like that. But you can alway start a new game - and be more careful.', 'new'],
];


/**
 * Here all the data is combined and handed over to the game.
 */
return [
    'startSettings' => $startSettings,
    'startMessages' => $startMessages,
    'endMessages' => $endMessages,
    'rooms' => $rooms,
    'allRooms' => $allRooms,
];
